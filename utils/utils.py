# @Author: Georgian Iosef <georgian>
# @Date:   2021-03-20T19:12:53+02:00
# @Email:  georgian4all@gmail.com
# @Filename: utils.py
# @Last modified by:   georgian
# @Last modified time: 2021-03-20T19:24:44+02:00

import json
import os
import sys

main_app_path = os.path.abspath(os.path.join(os.path.dirname(os.path.abspath(__file__)), '..'))


def collect_config():
    config_path = main_app_path + '/config.data'
    try:
        with open(config_path) as config_file:
            data = json.load(config_file)
    except Exception as e:
        print("ERROR: Config file is missing\n")
        print(e)
        sys.exit(1)
    return data
