#!/usr/bin/env python3
# @Author: Georgian Iosef <georgian>
# @Date:   2021-03-20T14:35:43+02:00
# @Email:  georgian4all@gmail.com
# @Filename: validation.py
# @Last modified by:   georgian
# @Last modified time: 2021-03-20T15:57:30+02:00
import re


def hex_color(hex_string):
    match = re.search(r'^#(?:[0-9a-fA-F]{3}){1,2}$', hex_string)
    return match
