#!/usr/bin/env python3
# @Author: Georgian Iosef <georgian>
# @Date:   2021-03-20T17:35:49+02:00
# @Email:  georgian4all@gmail.com
# @Filename: main.py
# @Last modified by:   georgian
# @Last modified time: 2021-03-25T13:12:08+02:00

from components import site_generator
import logging

logging.basicConfig(handlers=[logging.FileHandler(filename="./build.log",
                                                  encoding='utf-8', mode='a+')],
                    format="%(asctime)s %(name)s:%(levelname)s:%(message)s",
                    datefmt="%F  %T",
                    level=logging.INFO)


def main():
    logging.info("New build process started >> ")
    gallery = site_generator.SiteGenerator()
    gallery.generate_index()
    gallery.generate_robots_txt()
    gallery.generate_site_map()


if __name__ == "__main__":
    main()
