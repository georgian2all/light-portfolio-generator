# Light Portfolio Generator

Light and accessible portfolio site generator.


## Setup steps

### Install OS necessary libraries
    sudo apt install libjpeg-dev zlib1g-dev
    sudo apt-get install libfreetype6-dev
### Create python3 virtual environment and install requirements
    python3 -m venv venv
    source venv/bin/activate
    pip install --no-cache-dir -r requirements.txt

## Customize output using config.data file
    images-path - location where you are storing images intended to be organized as gallery.
                  Every images for a specific gallery must be stored in to individual subfolders
                  having names like the one you intend to be used for your gallery name.

## Launch website generator
    source venv/bin/activate
    python main.py

