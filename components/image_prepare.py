#!/usr/bin/env python3
# @Author: Georgian Iosef <georgian>
# @Date:   2021-03-16T17:33:26+02:00
# @Email:  georgian4all@gmail.com
# @Filename: image_prepare.py
# @Last modified by:   georgian
# @Last modified time: 2021-03-24T21:37:25+02:00

import glob
import json
import logging
import os
import shutil
import sys
from threading import Thread, Lock
from time import monotonic, sleep
from PIL import Image, ImageDraw, ImageFont
from random import randrange
from random import choice
from utils.validation import hex_color
from utils.utils import main_app_path, collect_config

thread = None
thread_lock = Lock()

logging.basicConfig(handlers=[logging.FileHandler(filename="./build.log",
                                                  encoding='utf-8', mode='a+')],
                    format="%(asctime)s %(name)s:%(levelname)s:%(message)s",
                    datefmt="%F  %T",
                    level=logging.INFO)


class ImagePrepare:
    def __init__(self):
        self.data = collect_config()
        self.validate_config()
        # If you are here, then config file is present and has the right format
        self.website_name = self.data["site-name"]
        self.main_app_path = main_app_path
        self.output = self.main_app_path + '/OUTPUT'
        if self.data["clean-project"] == 1:
            try:
                shutil.rmtree(self.output)
                sleep(0.02)
                os.makedirs(self.output)
            except OSError as e:
                try:
                    os.makedirs(self.output)
                except OSError as e:
                    logging.error("Can't create OUTPUT location")
                    logging.error(e)
                    sys.exit(1)

        self.generate_base_images = self.data["generate-base-images"]
        self.generate_thumbnails = self.data["generate-thumbnails"]
        if (self.generate_base_images != 1) and (self.generate_thumbnails != 1):
            logging.error("ERROR\nNothing to process.")
            logging.error("generate-base-images is not enabled")
            logging.error("generate-thumbnails is not enabled")
            logging.error("Can't create OUTPUT location")
            sys.exit(1)
        # Will store paths of new created galleries
        self.gallery_path = []

    def validate_config(self):
        # Validate content of configuration file and check if all needed
        # data to execute images processing are there
        number_parameters = ["generate-base-images", "image-basewidth",
                             "image-quality", "generate-thumbnails", "thumbnails-basewidth",
                             "thumbnails-quality", "font-ratio"]
        string_parameters = ["images-path", "site-name", "copy-right-text",
                             "font", "copy-right-text-color", "copy-right-text-fill"]
        needed_parameters = number_parameters + string_parameters
        available_parameters = self.data.keys()
        we_have_all_needed_parameters = all(elem in available_parameters for elem in needed_parameters)
        if not we_have_all_needed_parameters:
            logging.error("ERROR: Configuration file wrong formatting ")
            print("ERROR: Configuration file wrong formatting ")
            print("Example configuration content\n")
            print("{\n")
            for parameter in needed_parameters:
                if needed_parameters.index(parameter) != 12:
                    if parameter not in available_parameters:
                        print('"', parameter, '"', ": some_value, >> IS  MISSING FROM CURRENT CONFIGURATION FILE\n")
                    else:
                        print('"', parameter, '"', ": some_value,\n")
                else:
                    if parameter not in available_parameters:
                        print('"', parameter, '"', ": some_value, >> IS  MISSING FROM CURRENT CONFIGURATION FILE\n")
                    else:
                        print('"', parameter, '"', ": some_value\n")
            print("}\n")
            sys.exit(1)

    """
        Resize a given image file and set custom quality.
        If 0(zero) value is provided for minimum/basewidth then only
        image quality will be changed and keep original size (width/height).
    """

    def image_resize(self, image_file, basewidth, quality, what_to_generate, images_path, thumbnails_path):
        log_message = "Image resized: {} | Settings:  basewidth={};quality={}".format(image_file, basewidth, quality)
        logging.info(log_message)
        try:
            img = Image.open(image_file)
            original_w, original_h = img.size
            wpercent = (basewidth / float(img.size[0]))
            hsize = int((float(img.size[1]) * float(wpercent)))
            if basewidth == 0:
                img.thumbnail((original_w, original_h), Image.ANTIALIAS)
            else:
                img.thumbnail((basewidth, hsize), Image.ANTIALIAS)
            file_name = image_file.split('/')[-1]
            if what_to_generate == "base":
                save_path = images_path + '/{}'.format(file_name)
            elif what_to_generate == "thumb":
                save_path = thumbnails_path + '/{}'.format(file_name)
            img.save(save_path, optimize=True, quality=quality)
        except Exception as e:
            error_message = "cannot create thumbnail for {}".format(image_file)
            logging.error(error_message)
            logging.error(e)

    def _create_gallery_location(self, gallery_name):
        gallery_ = self.main_app_path + '/OUTPUT/{}/{}'.format(self.website_name, gallery_name)
        self.gallery_path.append(gallery_)
        thumbnails_path = gallery_ + '/media/thumbnails'
        images_path = gallery_ + '/media/images'
        thumbnails_exists = os.path.exists(thumbnails_path)
        images_exists = os.path.exists(images_path)
        error_makedirs = "Sound like we have some problem creating this folder: "
        if self.generate_thumbnails == 1:
            try:
                os.makedirs(thumbnails_path)
            except OSError as e:
                print("Thumbnails folder already there.\nAppend new images ...")
                pass
        if self.generate_base_images == 1:
            try:
                os.makedirs(images_path)
            except OSError as e:
                print("Images folder allready there.\nAppend new images ...")
                pass
        return images_path, thumbnails_path

    def _worker(self, source_images, gallery_name, basewidth, quality, what_to_generate):
        images_path, thumbnails_path = self._create_gallery_location(gallery_name)
        global thread
        image_extensions = ['jpg', 'JPG', 'PNG']
        counter = 0
        start_time = monotonic()
        for extension in image_extensions:
            pattern = '/*.{}'.format(extension)
            try:
                for filename in glob.iglob(source_images + pattern, recursive=False):
                    with thread_lock:
                        threads = []
                        thread = Thread(target=self.image_resize, args=(
                        filename, basewidth, quality, what_to_generate, images_path, thumbnails_path,))
                        threads.append(thread)
                        thread.start()
                    counter += 1
                for thread_ in threads:
                    thread_.join()
            # Used this exception to catch situation when some of our extensions
            # are not present in our images folder and face this kind of error
            # >>local variable 'threads' referenced before assignment<<
            # threads list will not be created if  no files exists

            except UnboundLocalError:
                # Extension not found
                pass
        end_time = monotonic()
        processing_time = (end_time - start_time)
        print("{} images processed in {} seconds.\n".format(counter, processing_time))

    """
        Resize and change image quality in bulk.
        Need to provide:
            - source_images - path to images to be added to the gallery
            - basewidth - minimum width for image scaling
            - quality -  image quality
            - what_to_generate - generate base image - "base"
                               - generate thumbnails - "thumb"
        Function call example:
            resize_bulk("images",0,80,"base")
            resize_bulk("images",3000,80,"thumb")
    """

    def resize_bulk(self):
        source_images = self.data["images-path"]  # check if directory really exists
        list_of_subfolders = list(os.walk(source_images))[0][1]
        if len(list_of_subfolders) == 0:
            logging.error("ERROR Nothing to process. Gallery folders are missing.")
            sys.exit(1)

        for folder in list_of_subfolders:
            if not os.listdir(source_images + f"/{folder}"):
                print("No files found in the directory.",folder)
                list_of_subfolders.remove(folder)
        logging.info("Images processing  >>  START")
        for gallery_name in list_of_subfolders:
            print("___")
            print("Processing gallery: {} ".format(gallery_name))
            if self.generate_base_images == 1:
                source_images_base = source_images + "/" + gallery_name
                print(source_images_base)
                print("Creating base images")
                what_to_generate = "base"
                basewidth = self.data["image-basewidth"]
                quality = self.data["image-quality"]
                self._worker(source_images_base, gallery_name, basewidth, quality, what_to_generate)

            if self.generate_thumbnails == 1:
                source_images_thumbnail = source_images + "/" + gallery_name
                print(source_images_thumbnail)
                print("Creating thumbnails")
                what_to_generate = "thumb"
                basewidth = self.data["thumbnails-basewidth"]
                quality = self.data["thumbnails-quality"]
                self._worker(source_images_thumbnail, gallery_name, basewidth, quality, what_to_generate)
        logging.info("Images processing >>  COMPLETE")
        galleries_unique_path = list(set(self.gallery_path))
        return galleries_unique_path

    def apply_copy_rights(self, image_file, temp_path, copy_text):
        img = Image.open(image_file)
        w, h = img.size

        # Make image editable
        drawing = ImageDraw.Draw(img)

        font_name = self.data["font"]
        font_path = "resources/fonts/{}".format(font_name)
        try:
            font_ratio = int(self.data["font-ratio"])
        except ValueError:
            # Wrong font-ratio provided in config file. Will use 48
            # Won't fail for such a small detail
            font_ratio = 48
        if w > h:
            font_size = int(w / font_ratio)
        else:
            font_size = int(h / font_ratio)
        font = ImageFont.truetype(font_path, font_size)

        # Text color
        text_color = self.data["copy-right-text-color"]
        if hex_color(text_color):
            font_color = "{}".format(text_color)
        else:
            font_color = "#000000"

        color_fill = self.data["copy-right-text-fill"]
        if hex_color(color_fill):
            fill_color = "{}".format(color_fill)
        else:
            fill_color = "#ffffff"

        # Text width and height
        copy_text = "" + copy_text + ""
        text_w, text_h = drawing.textsize(copy_text, font)

        # Text position - use random value for vertical position

        text_position = self.data["copy-right-text-position"]
        right_center = w - text_w, (int(h * 0.5) - text_h) - randrange(10, 15)
        right_down = w - text_w, (h - text_h) - randrange(10, 15)
        right_up = w - text_w, (int(h * 0.05) - text_h) - randrange(10, 15)
        available_positions = [right_up, right_center, right_down]
        if text_position == "random":
            position = choice(available_positions)
        elif text_position == "center":
            position = right_center
        elif text_position == "down":
            position = right_down
        else:
            position = right_up

        cp_text = Image.new('RGB', (text_w, (text_h)), color=font_color)
        drawing.text(position, copy_text, fill=fill_color, font=font)
        drawing = ImageDraw.Draw(cp_text)
        cp_text.putalpha(100)

        img.paste(cp_text, position, cp_text)
        file_name = image_file.split('/')[-1]
        image_path = temp_path + '/{}'.format(file_name)
        try:
            img.save(image_path)
        except Exception as e:
            logging.error(e)
            sys.exit(1)

    def copy_right_bulk(self):
        # Run only if base images are previously generated
        if self.generate_base_images == 1:
            logging.info("Apply copyright >>  START")
            global thread
            copy_right_text = self.data["copy-right-text"]
            output_location = self.output + '/{}'.format(self.website_name)
            try:
                list_of_subfolders = list(os.walk(output_location))[0][1]
            except IndexError:
                print("ERROR\n Nothing to process ... ")
            if len(list_of_subfolders) == 0:
                print("ERROR\n Nothing to process ... ")
            else:
                for gallery_name in list_of_subfolders:
                    temp_path = output_location + '/' + gallery_name + '/temp'
                    temp_exists = os.path.exists(temp_path)
                    if temp_exists:
                        shutil.rmtree(temp_path)
                        sleep(0.02)
                        os.makedirs(temp_path)
                    else:
                        try:
                            os.makedirs(temp_path)
                        except OSError as e:
                            logging.error(e)
                    try:
                        images_path = output_location + '/' + gallery_name + "/media/images"
                        images_list = glob.glob(images_path + '/*.*')
                        for image in images_list:
                            with thread_lock:
                                threads = []
                                thread = Thread(target=self.apply_copy_rights, args=(image, temp_path, copy_right_text))
                                threads.append(thread)
                                thread.start()
                        for thread_ in threads:
                            thread_.join()
                    except UnboundLocalError:
                        logging.error("COPYRIGHT >> OUTPUT folder is empty or no valid images are there.")
                    # Move al processed images from temp folder to base folder
                    source_dir = temp_path
                    dest_dir = images_path
                    try:
                        file_names = os.listdir(source_dir)
                        for file_name in file_names:
                            shutil.move(os.path.join(source_dir, file_name), os.path.join(dest_dir, file_name))
                    except Exception as e:
                        logging.error("COPYRIGHT >> ")
                        logging.error(e)
                    try:
                        shutil.rmtree(source_dir)
                    except Exception as e:
                        logging.error("COPYRIGHT >> ")
                        logging.error(e)
        logging.info("Apply copyright >>  COMPLETE")
