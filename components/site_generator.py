#!/usr/bin/env python3
# @Author: Georgian Iosef <georgian>
# @Date:   2021-03-20T17:31:21+02:00
# @Email:  georgian4all@gmail.com
# @Filename: site_generator.py
# @Last modified by:   georgian
# @Last modified time: 2021-03-25T14:02:41+02:00

from components import image_prepare
from datetime import date
import glob
from jinja2 import Environment, FileSystemLoader
import json
import logging
import os
import shutil
from utils.utils import main_app_path, collect_config

logging.basicConfig(handlers=[logging.FileHandler(filename="./build.log",
                                                  encoding='utf-8', mode='a+')],
                    format="%(asctime)s %(name)s:%(levelname)s:%(message)s",
                    datefmt="%F  %T",
                    level=logging.INFO)

today = date.today()


class SiteGenerator():
    def __init__(self):
        self.image = image_prepare.ImagePrepare()
        self.avilable_galleries = self.image.resize_bulk()
        self.image.copy_right_bulk()
        self.data = collect_config()
        self.main_app_path = main_app_path
        self.output = self.main_app_path + '/OUTPUT'
        self.site_name = self.data["site-name"]
        self.page_background = self.data["page-background"]
        self.gallery_root = self.output + "/" + self.site_name
        self.web_galleries = []
        self.add_resources()

    def add_resources(self):
        destination = self.gallery_root + '/resources'
        source = self.main_app_path + '/resources/website'
        shutil.copytree(source, destination, dirs_exist_ok=True)
        fav_ico_source = self.main_app_path + '/resources/favicon.ico'
        fav_ico_destination = self.gallery_root + '/favicon.ico'
        shutil.copy(fav_ico_source, fav_ico_destination)

    def generate_index(self):
        logging.info("Generate html files  >>  START")
        templates = self.main_app_path + '/templates'
        env = Environment(loader=FileSystemLoader(templates))
        template = env.get_template('index_gallery_tmplt.html')
        main_template = env.get_template('index_main_tmplt.html')
        all_galleries_images = []
        for gallery in self.avilable_galleries:
            my_g = gallery.split('/')[-1]
            self.web_galleries.append(my_g)
        for gallery in self.avilable_galleries:
            active_g = gallery.split('/')[-1]
            output_html = gallery + '/index.html'
            images = glob.glob(gallery + '/media/images/*.*')
            images_list = []
            for image in images:
                img = image.split('/')[-1]
                images_list.append(img)
            html = template.render(title="Gallery name", images=images_list, gallery_menu=self.web_galleries,
                                   active=active_g, site_name=self.site_name, page_background=self.page_background)
            with open(output_html, 'w') as out_file:
                out_file.write(html)
            all_galleries_images.append(images_list)

        html = main_template.render(title="Gallery name", images=all_galleries_images, galleries=self.web_galleries,
                                    site_name=self.site_name, page_background=self.page_background)
        output_html = self.gallery_root + '/index.html'
        with open(output_html, 'w') as out_file:
            out_file.write(html)
        logging.info("Generate html files  >>  COMPLETE")

    def generate_robots_txt(self):
        user_agent = "User-agent: *\nAllow: /\n"
        site_map = "Sitemap: https://artplace.ro/sitemap.xml"
        to_be_added = [user_agent, site_map]
        robots_file = self.gallery_root + '/robots.txt'
        with open(robots_file, 'w') as out_file:
            for data in to_be_added:
                out_file.write(data)

    def generate_site_map(self):
        generated_on = today.strftime("%Y-%m-%d")
        sitemap = self.gallery_root + '/sitemap.xml'
        templates = self.main_app_path + '/templates'
        env = Environment(loader=FileSystemLoader(templates))
        sitemap_template = env.get_template('sitemap.xml')
        xml_content = sitemap_template.render(galleries=self.web_galleries, site_name=self.site_name,
                                              site_update_date=generated_on)
        with open(sitemap, 'w') as out_file:
            out_file.write(xml_content)
